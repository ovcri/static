let activeDocument = null;
let allDocuments = {} // Store all the data for each spreadsheet
let filterParams = {}

function getJson(url, ok, err)
{
	
	let request = new XMLHttpRequest()
	request.open("GET", url);
	request.onerror = () => getJson(url, ok, err);
	request.onload = () => ok(request.responseText);
	request.send()

}

function createFedRow(rowdata)
{
	let template = document.getElementById("FedRowTemplate")
	let node = template.content.cloneNode(true)

	let rowTitle = node.querySelector(".rowTitle")
	let rowAgency = node.querySelector(".rowAgency")
	let rowAmount = node.querySelector(".rowAmount")
	let rowDeadline = node.querySelector(".rowDeadline")
	let rowEligibility = node.querySelector(".rowEligibility")
	let rowDescription = node.querySelector(".rowDescription")
	let rowProviderURL = node.querySelector(".rowProviderURL")	
		
	let rowStatus = node.querySelector(".Status")
	let rowStatusLabel = node.querySelector(".StatusLabel")

	rowTitle.textContent = rowdata.title
	rowAgency.textContent = rowdata.agency
	rowAmount.textContent = rowdata.amountCeil.trim() ? rowdata.amountCeil : (rowdata.amountFloor.trim() ? rowdata.amountFloor : (rowdata.estimatedFunding ? rowdata.estimatedFunding : "$--"))
	rowDeadline.textContent = rowdata.closeDate
	rowEligibility.innerHTML = rowdata.eligibility
	rowDescription.innerHTML = rowdata.description
	rowProviderURL.href = rowdata.url
		
	if (rowdata["new"]) {
		rowStatusLabel.innerHTML = "NEW"
		rowStatusLabel.classList.add("NewLabel")
		rowStatus.classList.add("NewStatus")
	}

	return node

}


function OpportunityButtonClass(node)
{
	let button = node.querySelector(".Opportunity")
	let details = node.querySelector(".Details")
	let isopen = false;
	
	let open = () =>
	{
		console.debug("button.open()")
		isopen = true;
		details.style.display = "block"
	}

	let close = () =>
	{
		console.debug("button.close()")
		isopen = false;
		details.style.display = "none"
	}

	let toggle = () => isopen ? close() : open()
	button.onclick = toggle
	return {
		open, toggle, close
	}
}

function RowElementClass(node)
{
	let open = () => node.style.display = "block"
	let close = () => node.style.display = "none"
	return { open, close }
}

function deleteAllChildren(parent)
{
	while (parent.firstChild) 
		parent.removeChild(parent.firstChild)
}

/* 
 * Returns function which wrapped by the current sort state
 * the `factory` arg is a callable which returns the sort parameter
 * */
function createSortFunction(sortA, sortB)
{
	
	let sort = 1;
	
	return () =>
	{
		
		let sortfn;
		if (sort)
			sortfn = sortA
		else
			sortfn = sortB
		if (currentDocument) {
			let data = currentDocument.data
			data.sort(sortfn)
			let parent = document.getElementById("Data")
			deleteAllChildren(parent)
			data.forEach(x => parent.appendChild(x.node))
			sort = !sort;
		}
	}
}


/* -- Callbacks to hide/show all funding opportunities -- */
document.getElementById("showAll").onclick = () => currentDocument.data.forEach((x) => x.button.open())
document.getElementById("hideAll").onclick = () => currentDocument.data.forEach((x) => x.button.close())


function getamount(c)
{

	let result 
	if (c.amountCeil.trim()) {
		result = parseFloat(c.amountCeil.replace(/[$,]+/g,""));
		
	}
	else if (c.amountFloor.trim()) {
		result = parseFloat(c.amountFloor.replace(/[$,]+/g,""));
	}
	else {
		result = parseFloat(c.estimatedFunding)
	}

	if (isNaN(result))
		return -1
	return result
	
}


const sortByDate = createSortFunction(
	(a, b) => new Date(a.closeDate) - new Date(b.closeDate),
	(a, b) => new Date(b.closeDate) - new Date(a.closeDate),
) // Create the actual sort function

const sortByTitle = createSortFunction(
	(a, b) => (a.title.localeCompare(b.title)),
	(a, b) => (b.title.localeCompare(a.title))
)

const sortByFunder = createSortFunction(
	(a, b) => (a.agency.localeCompare(b.agency)),
	(a, b) => (b.agency.localeCompare(a.agency)),
)

const sortByAmount = createSortFunction(
	(a, b) => (getamount(a) - getamount(b)),
	(a, b) => (getamount(b) - getamount(a)),
)


document.getElementById("sortDeadline").onclick = sortByDate
document.getElementById("sortTitle").onclick = sortByTitle
document.getElementById("sortFunder").onclick = sortByFunder
document.getElementById("sortAmount").onclick = sortByAmount

let handleResponse = (responseText) =>
{
	let grants = JSON.parse(responseText)
	let parentnode = document.getElementById("Data")
	deleteAllChildren(parentnode)	
	grants.data.forEach((row, i) => {
		let fragment = createFedRow(row)
		// Instanciate the fragment
		parentnode.appendChild(fragment)
		// Get the instanced fragment
		row.node = parentnode.children[i]						
		
		row.button = OpportunityButtonClass(row.node)
		row.thisobject = RowElementClass(row.node)
		
	})	
	deleteAllChildren(parentnode)
	return grants	
}


const PRETTY = {
	selectedFederalOpps: "Selected Federal Funding Opportunities (Excluding HHS)",
	selectedNonFederalOpps: "Selected Non-Federal Funding Opportunities",
	nonFederalOpps: "Pivot-RP Fund Opportunities Available in Three Months",
	allFederalOpps: "All Federal Fund Opportunities in Three Months",
}

/* Create the context for when the "New Only" check button is ticked. The purpose
 * of this button is to only show opportunities that were newly added. */
function createNewOnlyCallback()
{
	filterParams.newOnly = false
	return () => {  filterParams.newOnly = !filterParams.newOnly; applyCurrentFilter()}
}

document.getElementById("newOnly").onclick = createNewOnlyCallback()

function setCurrentDocument(name)
{
	let parent = document.getElementById("Data")
	deleteAllChildren(parent)
	if (allDocuments[name]) {
		currentDocument = allDocuments[name]
		document.getElementById("DocTitle").innerHTML = PRETTY[name]
		currentDocument.data.forEach((row) => parent.appendChild(row.node))
		let updated = document.getElementById("updated")
		updated.innerHTML = currentDocument.updateTime
		applyCurrentFilter()
	}
	else {
		parent.innerHTML = "Data Currently Unavailable"
		currentDocument = null;
	}
	
}

function initButton(elt, name)
{
	let html = document.getElementById(elt)
	html.onclick = () => setCurrentDocument(name)
	html.innerHTML = PRETTY[name]
}



document.getElementById("selectedFederalOpps").onclick = () => setCurrentDocument("selectedFederalOpps")
document.getElementById("nonFederalOpps").onclick = () => setCurrentDocument("nonFederalOpps")
document.getElementById("selectedNonFederalOpps").onclick = () => setCurrentDocument("selectedNonFederalOpps")
document.getElementById("AllFederalOpps").onclick = () => setCurrentDocument("allFederalOpps")

initButton("selectedFederalOpps", "selectedFederalOpps")
initButton("nonFederalOpps", "nonFederalOpps")
initButton("selectedNonFederalOpps", "selectedNonFederalOpps")
initButton("AllFederalOpps", "allFederalOpps")


getJson("https://ovcri.git-pages.mst.edu/data/selected_federal_opps.json",
	(responseText) => {
		allDocuments.selectedFederalOpps = handleResponse(responseText)
		setCurrentDocument("selectedFederalOpps")
		console.log("federal")
	},
	(request) => {
		
	}
)

getJson("https://ovcri.git-pages.mst.edu/data/non_federal_opps.json",
	(responseText) => {
		allDocuments.nonFederalOpps = handleResponse(responseText)
		setCurrentDocument("selectedFederalOpps")
		console.log("non federal")
	},
	(request) => {
		
	}
)


getJson("https://ovcri.git-pages.mst.edu/data/selected_non_federal_opps.json",
	(responseText) => {
		allDocuments.selectedNonFederalOpps = handleResponse(responseText)
		setCurrentDocument("selectedFederalOpps")
		console.log("selected non federal")
	},
	(request) => {
		
	}
)


getJson("https://ovcri.git-pages.mst.edu/data/all_federal_opps.json",
	(responseText) => {
		allDocuments.allFederalOpps = handleResponse(responseText)
		setCurrentDocument("selectedFederalOpps")
		console.log("all federal")
	},
	(request) => {
		
	}
)


/* -- Search -- */

function onsubmit(event)
{
	event.preventDefault()
	let searchtokens = event.target.elements["search"].value;
	if (searchtokens) {
		let tokens = searchtokens.split(";")
		filterParams.tokens = tokens
		applyCurrentFilter()
	}
}


const HIGHLIGHT_S = "<span class='highlight'>"
const HIGHLIGHT_E = "</span>"

function doKeywordSearch(tokens)
{
	let regexp = new RegExp(tokens.map((x) => x.toLowerCase()).join("|"), "g")
	
	currentDocument.data.forEach((x) => 
	{

		if (x.thisobject.clearHighlight) x.thisobject.clearHighlight()

		let elig = x.eligibility.toLowerCase()
		let descr = x.description.toLowerCase()
		let title = x.title.toLowerCase()
		let agency = x.agency.toLowerCase()

		let [match_elig, match_descr, match_title, match_agency, found] = [
			elig.matchAll(regexp),
			descr.matchAll(regexp),
			title.matchAll(regexp),
			agency.matchAll(regexp),
			elig.match(regexp) || descr.match(regexp) || title.match(regexp) || agency.match(regexp)
		]
		
		// Stores the current context of matches
		let matches = {
			eligibility:[],
			description:[]
		}
		
		x.thisobject.clearHighlight = () =>
		{
			// So that when this function is called, the context is cleared
				
			x.thisobject.clearHighlight = null;
		}
		
		// Ok so now we manually wrap the text with <span>regex</span>
		// This block is a noop if nothing is found
		for (let match of match_elig) {
			matches.eligibility.push(match) // save it so we can restore it later
			
		}

		for (let match of match_descr) {

		}
		
		if (match_elig) {				
			x.thisobject.clearHighlight = () => 
			{
				console.debug("clearing highlight")	
			}

		}

		if (found)
			x.thisobject.open()
		else 
			x.thisobject.close()
	})
}

function showNewOnly()
{
	currentDocument.data.forEach((x) => 
	{
		if (! x["new"]) x.thisobject.close()
	})
}

function clearSearch(event)
{
	filterParams.tokens = null
	applyCurrentFilter()
}

document.getElementById("clearSearch").onclick = clearSearch

function applyCurrentFilter()
{
	currentDocument.data.forEach((x) => x.thisobject.open())
	if (filterParams.tokens)
		doKeywordSearch(filterParams.tokens)
	if (filterParams.newOnly)
		showNewOnly()	
}

document.addEventListener("submit", onsubmit)
